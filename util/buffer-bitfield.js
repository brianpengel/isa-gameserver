"use strict";
/** @module*/

/**
 * Bitfield build on top of the Nodejs Buffer, variable in length but not dynamic in size.
 * @typedef {Bitfield} Bitfield
 * @constructor
 * @param   {(number|Buffer)} bitCount The length of the Bitfield in bits. Buffer: to be used as the base or Buffer to be used as the base.
 */
function Bitfield(bitCount){
	var buff = null ;
	if (Buffer.isBuffer(bitCount)) {
		buff = bitCount ;
		bitCount = buff.length * 8 ;
	}
	else {
		buff = Buffer.alloc(CalcByteCount(bitCount));
	}
	
	/**
	* Set an individual bit, bits are set left to right.
	* @function setBit
	* @param {number} index The index of the bit to be set
	* @param {bool|number} val The value of the bit to be set, either 1 or 0
	*/
    this.setBit =(index, val)=> {
        var byte = index >>> 3,
			mask = 128 >> (index % 8) ;
        if(val)
             buff[byte] |= mask ;
        else buff[byte] &= mask ;
    };
    
	/**
	* Get an individual bit, bits are read left to right.
	* @function getBit
	* @param {number} index The index of the bit to be read
	* @returns {number} The value of the bit, either 1 or 0.
	*/
    this.getBit =(index) => {
        var byte = index >>> 3,
			mask = 128 >> (index % 8) ;
		return buff[byte] & mask ;
    };
	
	/**
	* Shift every bit by the specified amount.
	* @function shift
	* @param {number} amount The amount in bits the buffer should be shift.
	*/
	this.shift =(amount)=> {
		if(amount >= bitCount) {
			buff.fill(0);
			return ;
		}
		
		amount >>>= 0 ;
		amount = Math.min(amount, bitCount-1);
		var bytesToShift = amount >> 3,
			bitsToShift = amount % 8 ;
			
		ShiftBytes(bytesToShift) ;
		ShiftBits(bitsToShift, bytesToShift) ;

	};
	
	function ShiftBytes(amount) {
		if(amount < 1)
			return ;
		
		var val = buff.slice(0, -amount);
		buff.fill(val, amount);	
		buff.fill(0, 0, amount);
	};
	
	function ShiftBits(amount, offset) {
		var remembered = 0,
			mask = 0 ;
		
		for(var cnt = 0; cnt < amount; cnt ++)
			mask = mask | 1 << cnt;
		
		for(var cnt = offset - 1; cnt < buff.length; cnt ++){
			var val = buff[cnt], 
				tmp = val & mask;
				
			buff[cnt] >>= amount ;
			buff[cnt] |= remembered << (8 - amount) ;
			remembered = val & mask;
		}
	};
	
	/**
	* Fills the entire bitfield with 1's
	* @function fill
	*/
	this.fill =()=> {
		buff.fill(256 * buff.length - 1) ;
	};
	
	Object.defineProperty(this, 'buffer', { get: function() { return buff ; } }) ;
	Object.defineProperty(this, 'length', { get: function() { return bitCount; } }) ;
	Object.defineProperty(this, 'octetLength', { get: function() { return buff.length ; } }) ;
};

/**
* Checks if object is a Bitfield.
* @function isBitfield
* @param {object} obj The object to compare.
*/
Bitfield.isBitfield =(obj)=> {
	return obj instanceof Bitfield ;
};

function CalcByteCount (bitCount) {
    var amount = bitCount >> 3 ;
    if(bitCount % 8 !== 0) amount ++ ;
    return amount ;
};

module.exports = Bitfield ;