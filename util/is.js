/** @module*/

/** 
 * Several functions to test for an objects type.
 * @type {object} 
 */

var is = module.exports = {
    'function': (functionToCheck) => {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    },
    
    'object': (val) => { return Object.is(val, {}); },    
    'string': (val) => { return typeof val === 'string'; },
    'number': (val) => { return typeof val === 'number'; },
    'required': (name) => { throw new Error(`param (${name}) is required`); }
};