"use strict";
/** @module */
const is = require('./is') ;

/**
 * Original bitfield, limited to 8, 16 or 32 bits
 * @deprecated Use util/buffer-bitfield instead.
 * @param   {number} bits  The amount of bits.
 * @param   {string} read  The name of the function used to read the buffer.
 * @param   {string} write The name of the function used to write to the buffer.
 */
function IntBasedBitfield (bits, read, write) {
    var bytes = bits >> 3 ;
    var mask = Math.pow(2, bits) - 1;
    
    // Makeshift bitfield using an integer
    // TODO: Create implementation using buffers - check npm bitfield for reference
    function bfield(startVal){  
        if(is.string(startVal))
            startVal = parseInt(startVal, 2) ;

        var field = startVal >>> 0 ;

        this.setBit = (index, val) => {
            if(val)
                 field |= 1 << index ;
            else field &= ~1<< index ;
        };

        this.isset = (index) => {
            return (field & 1 << index) != 0 ;
        };

        this.rShiftField = (amount) => {
            amount = amount | 1 ;
            field = field >> amount ;
        };

        this.lShiftField = (amount) => {
            amount = amount | 1 ;
            field = field << amount ;
        };

        // dec2bin
        this.toString = () => {
            var n = fieldUInt().toString(2) ;        
            return '0'.repeat(bits).substr(n.length)+n ;
        };

        this._dec = () => {     
            return fieldUInt() ;
        };

        this.toBuffer = (buf, offset)=> {
            offset = offset || 0 ;
            if(!Buffer.isBuffer(buf))
                buf = Buffer.alloc(bytes);
            buf[write](fieldUInt(), offset);
            return buf ;
        };

        function fieldUInt() {
            return field = (field & mask) >>> 0 ;
        };
        
        Object.defineProperty(this, 'length', {
            get: function() { return bytes ; }
        })
    };
    
    bfield.fromBuffer = (buf) => {
        if(!Buffer.isBuffer(buf))
            return new bfield() ;
        return new bfield(buf[read]()) ; ;
    };
    
    return bfield ;
};



module.exports = {
    'bitfield8' : IntBasedBitfield ( 8,    'readUInt8', 'writeUInt8'),
    'bitfield16': IntBasedBitfield (16, 'readUInt16LE', 'writeUInt16LE'),
    'bitfield32': IntBasedBitfield (32, 'readUInt32LE', 'writeUInt32LE')
} ;