"use strict";

var ChainableList = function (opt) {
	var self = this,
		nodes = [] ;
	
	opt.types.forEach(function(type) {
		Object.defineProperty(self, type, {value: (name, parameters)=>{
			nodes.push({
				name: name, 
				type: type, 
				params: parameters
			}) ;
			
			return self ;
		}}) ;
	});
	
	function Traverse (callbacks) {
		var globals = {},
			result = {} ;
		
		nodes.forEach(function(cur) {
			result[cur.name] = callbacks[cur.type](cur.params, globals);
		});
		
		return result ;
	};
	
	Object.defineProperty(this, "Traverse", {value: Traverse}) ;
	Object.defineProperty(this, "nodes", {get: ()=>{ return nodes ; }}) ;
};

/**
 * Creates a new Node List
 * @param {Array} types  [[Description]]
 * @param {object} logger [[Description]]
 */
ChainableList.Create = function (types, logger) {
	types = types || [];
	logger = logger || {
		log: console.log.bind(console),
		warn: console.warn.bind(console),
		info: console.info.bind(console),
		error: console.error.bind(console),
	} ;
	
	return new ChainableList({
		types: types,
		logger: logger
	}) ;
};


module.exports = ChainableList ;