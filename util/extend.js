/** @module*/

/**
 * A method used for inheritance.
 * @function Extend
 * @param   {object} parent The object to inherit from.
 * @param   {object} child  The object to apply the inheritance to.
 * @returns {object} The altered child object.
 */
var extend = module.exports = function Extend (parent, child) {
	child.super_ = parent;
	child.prototype = Object.create(parent.prototype, { constructor: { value: child, enumerable: false } });
	return child ;
};

// Call to execute parent constructor
//  child.super_.apply(this, arguments);