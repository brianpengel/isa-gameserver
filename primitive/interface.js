const dgram     = require('dgram'),
      extend    = require('../util/extend'),
	  Socket	= dgram.Socket ;

var Interface = extend(Socket, function Interface(type) {
    Interface.super_.call(this, type);
	var openConnections = {} ;
	
	this.on('listening', ()=>{
		var address = this.address();
		console.log('// ==================================================== //');
        console.log(`Listening on ${address.address}:${address.port}`);
        console.log('// ==================================================== //');
	});
	
//	this.on('close', ()=>{
//		var address = this.address();
//		console.log('// ==================================================== //');
//        console.log(`Stop listening on ${address.address}:${address.port}`);
//        console.log('// ==================================================== //');
//	});
});

module.exports = Interface;