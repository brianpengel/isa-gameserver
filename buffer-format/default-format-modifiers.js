"use strict";

module.exports = {
	endian: (param, global)=>{ 
		global.endian = param == "le" ? "le" : "be" ;
		global.endian = global.endian.toUpperCase() ;
	},
	skip: (param, global)=>{ global.index += param ; },
};