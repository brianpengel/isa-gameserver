"use strict";

const Bitfield = require("../util/buffer-bitfield") ;

module.exports = {
	// Int
	int8: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index ++ ;
		return buff.readInt8(index) ;
	},
	
	int16: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += 2 ;
		return buff["readInt16" + global.endian](index) ;
	},
	
	int32: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += 4 ;
		return buff["readInt32" + global.endian](index) ;
	},
	
	// UInt
	uInt8: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index ++ ;
		return buff.readUInt8(index) ;
	},
	
	uInt16: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += 2 ;
		return buff["readUInt16" + global.endian](index) ;
	},
	
	uInt32: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += 4 ;
		return buff["readUInt32" + global.endian](index) ;
	},
	
	// Float
	float: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += 4 ;
		return buff["readFloat" + global.endian](index) ;
	},
	
	// Double
	double: (param, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += 8 ;
		return buff["readDouble" + global.endian](index) ;
	},
	
	// Bitfield
	bitfield: (length, global)=>{
		var buff = global.target,
			index = global.index ;
		
		global.index += length ;
		return new Bitfield(buff.slice(index, index + length)) ;
		
	},
} ;