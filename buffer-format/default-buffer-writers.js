"use strict";

const Bitfield = require("../util/buffer-bitfield") ;

module.exports = {
	// Int
	int8: (param, global, val)=>{
		var buff = global.target ;
		buff.writeInt8(val, global.index) ;
		global.index ++ ;
	},
	
	int16: (param, global, val)=>{
		var buff = global.target ;
		buff["writeInt16" + global.endian](val, global.index) ;
		global.index += 2 ;
	},
	
	int32: (param, global, val)=>{
		var buff = global.target ;
		buff["writeInt32" + global.endian](val, global.index) ;
		global.index += 4 ;
	},
	
	// UInt
	uInt8: (param, global, val)=>{
		var buff = global.target ;
		buff.writeUInt8(val, global.index) ;
		global.index ++ ;
	},
	
	uInt16: (param, global, val)=>{
		var buff = global.target ;
		buff["writeUInt16" + global.endian](val, global.index) ;
		global.index += 2 ;
	},
	
	uInt32: (param, global, val)=>{
		var buff = global.target ;
		buff["writeUInt32" + global.endian](val, global.index) ;
		global.index += 4 ;
	},
	
	// Float
	float: (param, global, val)=>{
		var buff = global.target ;
		buff["writeFloat" + global.endian](val, global.index) ;
		global.index += 4 ;
	},
	
	// Double
	double: (param, global, val)=>{
		var buff = global.target ;
		buff["writeDouble" + global.endian](val, global.index) ;
		global.index += 8 ;
	},
	
	// Bitfield
	bitfield: (length, global, val)=>{
		if(!Bitfield.isBitfield(val))
			return ;
		
		var buff = global.target ;
		val.buffer.copy(buff, global.index, 0, val.buffer.length) ;
		global.index += length ;
	},
} ;