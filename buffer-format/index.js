"use strict";
/** @module buffer-format */

const readers = require("./default-buffer-parsers"),
	  writers = require("./default-buffer-writers"),
	  modifiers = require("./default-format-modifiers") ;

/**
 * A template for reading and writing buffers.
 * @constructor
 * @param   {object}       modifiers   Methods to modify the behaviour of the BufferFormat.Read and BufferFormat.Write methods
 * @param   {object}       [writers={] Methods to instruct how to write to the buffer.
 * @param   {object}       [readers={] Methods to instruct how to read from the buffer.
 */
function BufferFormat (modifiers, writers, readers) {
	var self = this,
		nodes = [] ;
	
	(Object.keys(Object.assign({}, writers, readers)))
		.forEach((function(type) {
			Object.defineProperty(this, type, {value: (name, parameters)=>{
				nodes.push({ name: name, type: type, params: parameters }) ;
				return this ;
			}}) ;
		}).bind(this));
	
	Object.keys(modifiers).forEach((function(type) {
		Object.defineProperty(this, type, {value: (parameters)=>{
			nodes.push({ type: type, params: parameters }) ;
			return this ;
		}}) ;
	}).bind(this));
	
	Object.assign(writers, modifiers) ;
	Object.assign(readers, modifiers) ;
	
	
	/**
	 * Traverse buffer (buffer) and write values acording to format.
	 * @param   {Buffer} buffer The buffer to traverse.
	 * @param   {object} values The values to be written to the buffer
	 * @returns {Buffer} The buffer written to.
	 */
	function Write (buffer, values) {
		if(!Buffer.isBuffer(buffer) || values == undefined)
			return null ;
	
		var globals = {target: buffer, index: 0};
		
		nodes.forEach(function(cur) {
			writers[cur.type](cur.params, globals, values[cur.name]);
		});
		
		return buffer ;
	};
	
	/**
	 * Traverse buffer (buffer) and read values acording to format.
	 * @param   {Buffer} buffer The buffer to traverse.
	 * @returns {object} The results sorted by name.
	 */
	function Read (buffer) {
		if(!Buffer.isBuffer(buffer))
			return null ;
	
		var globals = {target: buffer, index: 0},
			result = {} ;
		
		nodes.forEach(function(cur) {
			var res = readers[cur.type](cur.params, globals);
			if(cur.name) result[cur.name] = res ;
		});
		
		return result ;
	};
	
	Object.defineProperty(this, "nodes", {get: ()=>{ return nodes ; }}) ;
	Object.defineProperty(this, "Write", {value: Write}) ;
	Object.defineProperty(this, "Read", {value: Read}) ;
};

/**
* Creates a new BufferFormat with the default parsers and writers.
* @returns {BufferFormat} The created format.
*/
Object.defineProperty(BufferFormat, "CreateDefault", { 
	value: function() {
		return new BufferFormat (
			modifiers,
			writers,
			readers
		) ;
	}
});

module.exports = BufferFormat ;