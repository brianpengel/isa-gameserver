BufferFormat allows users to define schema's through chainable methods,
that can be used to read/write data to/from Buffers.

The following schema:
```js
const BufferFormat = require("./util/buffer-format");

var newFormat = BufferFormat.CreateDefault()
      .endian("le")
      .uInt8 ("a")
      .uInt8 ("b")
      .uInt16("c")
      .uInt32("d")
      .bitfield("e", 4);
```

Would return the following object, when reading from a buffer using ``newFormat.Read(message)``.
```js
{
  "a": number(uInt8),
  "b": number(uInt8),
  "c": number(uInt16),
  "d": number(uInt32),
  "e": object(bitfield) // with length 4
}
```

And can be used to write to a buffer using ``newFormat.Write(message, values)``.
```js
const Bitfield = require("./util/buffer-bitfield") ;

var ackfield = new Bitfield(32) ;
var message = Buffer.alloc(8 + ackField.octetLength,'binary') ;
var values = {
    a: 55,
    b: 1,
    c: 42,
    d: 0,
    e: ackField
};

// Writing values to buffer
newFormat.Write(message, values) ;
```
