const dgram     = require('dgram'),
      extend    = require('../util/extend'),
      Connection= require('./udp-connection'),
	  Socket	= dgram.Socket ;

var WrapperUDP = extend(Socket, function WrapperUDP(type) {
    WrapperUDP.super_.call(this, type);
	var openConnections = {} ;
	
	// 
	this.on('listening', ()=>{
		var address = this.address();
		
		console.log('// ==================================================== //');
        console.log(`Server listening ${address.address}:${address.port}`);
        console.log('// ==================================================== //');
	});
	
	this.on('message', (msg, rInfo)=>{
		var id = rInfo.address + ':' + rInfo.port ;
		if(openConnections[id] != undefined) {
			openConnections[id].onMessage(msg) ;
			return ;
		}
		
		console.log(rInfo);
	});
	
});

module.exports = WrapperUDP;



//function udpWrapper (h, p) {
//    
//    var host = h || 'localhost', 
//        port = p || 0, 
//        listening = false ;
//    
//    server.on('listening', onListening);
//    server.on('error', onError);
//    // Debug
//    server.on('message', (msg, rinfo)=>{ console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);});
//    
//    
//
//    function onListening () {
//        var address = server.address();
//        listening = true ;
//
//        console.log('// ====================================================== //');
//        console.log(`Server listening ${address.address}:${address.port}`);
//        console.log('// ====================================================== //');
//    };
//
//    function onError (err) {
//        // Replace with error log(file) implementation
//        console.log(`server error:\n${err.stack}`);
//        server.close();
//    };
//    
//    this.CloseSocket() => {
//        if(!listening)
//            return ;
//
//        server.close(port); 
//        console.log('-) Closed port | ', port) ;
//    };
//
//    this.RestartSocket() => {
//        CloseSocket () ;
//        server.bind(port, host);
//    };
//};