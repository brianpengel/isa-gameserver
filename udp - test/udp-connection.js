"use strict";

const Wrapper = require('../buffer-wrapper'),
	  extend = require('../util/extend'),
	  EventEmitter = require('events');

// /////////////////////////////////////////////// //
// Packet Head Layout
// /////////////////////////////////////////////// //
/*
 * [uint protocol id]	// 0 - 255
 * [uint sequence]		// 0 - 255
 * [uint ack]			// 0 - 255
 * [uint ack bitfield]	// 32 bits / 4 octet
 * [uint message type]	// 0 - 255
 * (data)
*/

const maxUInt8 = 255,
	  headLength = 8,
	  headParser = Wrapper.create()
		.uint8('protocolID')
		.uint8('remoteSeq')
		.uint8('ack')
		.bitfield('ackField', { 
			length: 4
		})
		.uint8('type') ;


// /////////////////////////////////////////////// //
// Connection Class
// /////////////////////////////////////////////// //

var Connection = extend(EventEmitter, function Connection(protocolID, targetHost, targetPort) {
	Connection.super_.apply(this);
	
	var localSeq = 0,
		remoteSeq = 0,
		messageIDs = [],
		messageRoutes = {},
		ackfield = new Wrapper.Bitfield(32);
		ackfield.fill() ;
	
	// ===< Routing >=== //
	var onMessage =(data)=> {
		if(messageRoutes.length == 0)
			return ;
		
		var head = headParser.parse(data),
			messageName = null ;
		
		if(
			!sequenceMoreRecent(head.remoteSeq, remoteSeq, maxUInt8) || 
			head.type == undefined || 
			!(messageName = messageIDs[head.type])
		  ) return ;
		
		this.emit(messageName, messageRoutes[messageName].parser.parse(data.slice(headLength)), head);
		var diff = sequenceDifference(head.remoteSeq, remoteSeq, maxUInt8);
		
		// Update remote data
		remoteSeq = head.remoteSeq ;
		ackfield.shift(diff) ;
		ackfield.setBit(0, true) ;
	};
	
	this.addRoute =(name, parser, dataLength)=>{
		messageIDs.push(name);
		messageRoutes[name] = {};
		messageRoutes[name].id = messageIDs.length - 1 ;
		messageRoutes[name].parser = parser;
		messageRoutes[name].buffer = Buffer.alloc(headLength + dataLength);
		messageRoutes[name].dataBuff = messageRoutes[name].buffer.slice(headLength);
	};
	
	// ===< Messages >=== //
	function WriteHeader(buff, type) {
		headParser.write(buff, {
			protocolID: protocolID,
			remoteSeq: localSeq,
			ack: remoteSeq,
			ackField: ackfield,
			type: type
		}) ;
	};
	
	this.send =(name, data)=>{
		localSeq = (localSeq + 1) % (maxUInt8 + 1) ;
		WriteHeader(messageRoutes[name].buffer, messageRoutes[name].id);
		messageRoutes[name].parser.write(messageRoutes[name].dataBuff, data) ;
//		console.log('| SEND MESSAGE |', messageRoutes[name].buffer) ;
//		onMessage(messageRoutes[name].buffer) ;
	};
	
	// ===< Sequence Management >=== //
	function sequenceMoreRecent(newSeq, oldSeq, max) {
        return ( newSeq > oldSeq ) && ( newSeq - oldSeq <= max/2 ) ||
               ( oldSeq > newSeq ) && ( oldSeq - newSeq  > max/2 );
    };
    
    function sequenceDifference(newSeq, oldSeq, mask) {
        return (newSeq - oldSeq) >>> 0 & mask ;
    };
});

module.exports = Connection ;