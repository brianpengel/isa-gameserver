"use strict";
const Interface = require("../primitive/interface"),
	  BufferWrapper = require("../buffer-format"),
	  Bitfield = require("../util/buffer-bitfield");

var msgFormat = BufferWrapper.CreateDefault(),
	ackField = new Bitfield(32) ;

ackField.fill() ;
ackField.shift(16) ;
ackField.setBit(1, 1) ;

ackField.setBit(5, 1) ;
	
msgFormat.endian("le")
		.uInt8 ("a")
		.uInt8 ("b")
		.uInt16("c")
		.uInt32("d")
		.bitfield("e", ackField.octetLength);

((client, port, ip)=>{
	ip = ip || 'localhost' ;
	client.bind(port, ip) ;
	
	client.on('message', (msg, rInfo)=>{
		console.log(msg, rInfo);
	});
	
	function onReturn (id) {
		client.close(port) ;
		clearInterval(id) ;
	};
	
	
	var message = Buffer.alloc(8 + ackField.octetLength,'binary');
	var values = {
		a: 55,
		b: 1,
		c: 42,
		d: 0,
		e: ackField
	};
	
	
//	var message = Buffer.from(JSON.stringify({"route":"shoutout"}),'binary');
	var intervalID = setInterval(()=>{
		values.d += 1 ;
		msgFormat.Write(message, values) ;
		console.log(message);
		client.send(message, 0, message.length, port, ip); 
	}, 1);
	setTimeout(onReturn, 5000, intervalID);
	
})(new Interface({ type: 'udp4', reuseAddr: true }), 41235);