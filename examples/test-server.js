"use strict";

const Interface = require("../primitive/interface"),
	  BufferWrapper = require("../buffer-format");

var msgFormat = BufferWrapper.CreateDefault() ;
msgFormat.endian("le")
		.uInt8 ("a")
		.uInt8 ("b")
		.uInt16("c")
		.uInt32("d")
		.bitfield("e", 4);

((server, port, ip)=>{
	ip = ip || 'localhost' ;
	server.bind(port, ip) ;

	var openConnections = {} ;
	
	server.on('message', (msg, rInfo)=>{
//		var id = rInfo.address + ':' + rInfo.port ;
//		if(openConnections[id] != undefined) {
//			openConnections[id].onMessage(msg) ;
//			return ;
//		}
		
		console.log("Message from", rInfo.address + ':' + rInfo.port);
		var res = msgFormat.Read(msg) ;
		res.e = res.e.buffer.toString("hex") ;
		console.log(res);
	});
	
	setTimeout(server.close.bind(server), 10000, port); // Automatically stop server after 10 seconds
	
})(new Interface({ type: 'udp4', reuseAddr: true }), 41235);

console.log(Buffer);
console.log(9%8?"yes":"no");